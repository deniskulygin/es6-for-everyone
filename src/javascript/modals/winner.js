import {showModal} from "./modal";
import {createElement} from "../helpers/domHelper";

export  function showWinnerModal(winner) {
    const title = 'Winner info';
    const bodyElement = createWinnerDetails(winner);
    showModal({ title, bodyElement });
}

function createWinnerDetails(winner) {
    const { name } = winner;

    const winnerDetails = createElement({ tagName: 'div', className: 'modal-body' });
    const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

    nameElement.innerText = name;
    winnerDetails.append(nameElement);

    return winnerDetails;
}