export function fight(firstFighter, secondFighter) {
    while (firstFighter.health > 0 || secondFighter.health > 0)
    {
        let attacker;
        let enemy;

        if (firstKick) {
          attacker = firstFighter;
          enemy = secondFighter;
        } else {
          attacker = secondFighter;
          enemy = firstFighter;
        }

        enemy.health -= getDamage(attacker, enemy);

        if (enemy.health <= 0) {
            return attacker;
        }

        firstKick = !firstKick;
    }
}

let firstKick = true;

export function getDamage(attacker, enemy) {
    return Math.max(getHitPower(attacker) - getBlockPower(enemy));
}

export function getHitPower(fighter) {
    const criticalHitChance = randomIntFromInterval(1, 2);
    return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
    const dodgeChance = randomIntFromInterval(1, 2);
    return fighter.defense * dodgeChance;
}

function randomIntFromInterval(min, max) {
    return Math.random() * (max - min) + min;
}
